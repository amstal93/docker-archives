#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"

VOLUMES=()
VOLUMES=("${VOLUMES[@]}" "ddidier-sonarqube_sonarqube-conf")
VOLUMES=("${VOLUMES[@]}" "ddidier-sonarqube_sonarqube-data")
VOLUMES=("${VOLUMES[@]}" "ddidier-sonarqube_sonarqube-extensions")
VOLUMES=("${VOLUMES[@]}" "ddidier-sonarqube_sonarqube-bundled-plugins")
VOLUMES=("${VOLUMES[@]}" "ddidier-sonarqube_postgres")
VOLUMES=("${VOLUMES[@]}" "ddidier-sonarqube_postgres-data")



function sonarqube() {
    action="$1"

    case $action in
        init)    _sonarqube_init;;
        size)    _sonarqube_size;;
        start)   _sonarqube_start;;
        stop)    _sonarqube_stop;;
        *)       _sonarqube_help;;
    esac
}

function _sonarqube_init() {
    docker network create ddidier-sonarqube-net

    # pushd $PROJECT_DIR && docker-compose build ; popd

    for volume in ${VOLUMES[@]}; do
        docker volume create --name $volume
    done
}

function _sonarqube_size() {
    for volume in ${VOLUMES[@]}; do
       echo "$volume = $(sudo du -hs /var/lib/docker/volumes/$volume | cut -f 1)"
    done
}

function _sonarqube_start() {
    pushd $PROJECT_DIR && docker-compose up ; popd
}

function _sonarqube_stop() {
    pushd $PROJECT_DIR && docker-compose down ; popd
}

function _sonarqube_help() {
    echo "Usage: ./sonarqube.sh init|size|start|stop"
    echo "  init .... build the images and create the persistent volumes and the external networks"
    echo "  size .... display the persistent volumes size"
    echo "  start ... start all the containers"
    echo "  stop .... stop all the containers"
}

sonarqube $@
