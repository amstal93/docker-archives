# NDD Docker ELK

## Version es241_l240_k461

- Update versions to ElasticSearch 2.4.1, Logstash 2.4.0, Kibana 4.6.1

## Version es235_l234_k454

- Update versions to ElasticSearch 2.3.5, Logstash 2.3.4, Kibana 4.5.4

## Version es233_l232_k451

- Initial commit with versions ElasticSearch 2.3.3, Logstash 2.3.2, Kibana 4.5.1
