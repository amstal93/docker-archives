# NDD Docker Atom

A Docker image for the [Atom editor](https://atom.io/).

The image is based upon the official [ubuntu:xenial](https://hub.docker.com/_/ubuntu/).

Some basic plugins are also installed:

- [highlight-selected](https://atom.io/packages/highlight-selected)
- [minimap](https://atom.io/packages/minimap)
- [minimap-highlight-selected](https://atom.io/packages/minimap-highlight-selected)



## Installation

### From source

```
git clone git@bitbucket.org:ndd-docker/ndd-docker-atom.git
cd ndd-docker-atom
docker build -t ddidier/atom .
```

### From Docker Hub

```
docker pull ddidier/atom
```



## Usage

Atom will be executed by the `atom` user which is created by the Docker entry point. You **must** pass to the container the environment variable `USER_ID` set to the UID of the user the files will belong to. For example ``-e USER_ID=`id -u $USER` ``.

The settings are stored in the container in the directory `/home/atom`. You have multiple choices to keep them:

- never delete your container
- use a data volume e.g. `-v $HOST_DATA_DIR:/home/atom`
- use a data volume container e.g. `--volumes-from atom-data`

If you store these data in a directory like `~/.docker-data/<CONTAINER_NAME>`, you can run this image using:

```bash
docker run --rm                       \
    -v /tmp/.X11-unix:/tmp/.X11-unix  \
    -v <HOST_DATA_DIR>:/data          \
    -v ~/.docker-data/atom:/home/atom \
    -e DISPLAY=$DISPLAY               \
    -e USER_ID=`id -u $USER`          \
    --name atom                       \
    ddidier/atom
```
