#
# ddidier/bitbake
#
# A Docker image for BitBake distribution tool.
#
# docker build -t ddidier/bitbake .

FROM       python:3.7.3-stretch
MAINTAINER David DIDIER

ENV BITBAKE_VERSION=1.42.0

RUN export DEBIAN_FRONTEND=noninteractive \
 && apt-get update \
 && apt-get install -y --no-install-recommends gosu sudo \
 && apt-get autoremove -y \
 && rm -rf /var/cache/* \
 && rm -rf /var/lib/apt/lists/* \
 \
 && wget --directory-prefix=/tmp/ http://git.openembedded.org/bitbake/snapshot/bitbake-${BITBAKE_VERSION}.tar.gz \
 && tar xfz /tmp/bitbake-${BITBAKE_VERSION}.tar.gz -C /opt \
 && ln -s bitbake-${BITBAKE_VERSION} /opt/bitbake \
 \
 && echo "" >> /etc/skel/.bashrc \
 && echo "PATH=/opt/bitbake/bin:$PATH" >> /etc/skel/.bashrc \
 && echo "PYTHONPATH=/opt/bitbake/lib:$PYTHONPATH" >> /etc/skel/.bashrc \
 && sed -i 's/#alias/alias/g' /etc/skel/.bashrc

COPY files/usr/local/bin/*      /usr/local/bin/
COPY files/usr/share/ddidier/*  /usr/share/ddidier/

ENV DATA_DIR=/data

WORKDIR $DATA_DIR

ENTRYPOINT ["/usr/local/bin/docker-entrypoint"]
