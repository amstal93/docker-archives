# NDD Docker Percona Toolkit

A Docker image for the [Percona Toolkit](https://www.percona.com/software/mysql-tools/percona-toolkit).

The image is based upon the official [debian:jessie] image.

## Installation

### From source

```
git clone git@bitbucket.org:ndd-docker/ndd-docker-percona-toolkit.git
cd ndd-docker-percona-toolkit
docker build -t ddidier/percona-toolkit .
```

### From Docker Hub

```
docker pull ddidier/percona-toolkit
```



## Usage

The data directory on the host `<HOST_DATA_DIR>` must be mounted as a volume under `/data` in the container.

Use `-v <HOST_DATA_DIR>:/data` to use a specific data directory or `-v $(spec):/data` to use the current directory as the data directory.

The container is executed by the user `percona` belonging to the group of the `<HOST_DATA_DIR>` directory. All new files will thus belong to this group.

### Interactive

```
docker run -i -t -v <HOST_DOC_DIR>:/data ddidier/percona-toolkit
```

You should now be in the `/data` directory, otherwise just `cd` to `/data`.

You can now call any Percona script, for example:

```
pt-query-digest /data/mysqld-slow.log > /data/mysqld-slow-report.txt
```

### Non interactive

TODO
