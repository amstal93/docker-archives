#!/bin/bash

set -e

#
# Build the Docker image and push it to the GitLab registry.
#
# Parameters:
#
# $CI_COMMIT_SHA
# $CI_REGISTRY
# $CI_REGISTRY_IMAGE
# $CI_REGISTRY_PASSWORD
# $CI_REGISTRY_USER
#
# @version 0.1.2
# @author David DIDIER
# @see https://gitlab.com/ddidier/docker-tools
#

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source $SCRIPT_DIR/commons.sh

print_h1 "Build the Docker image and push it to the GitLab registry"

print_h2 "Script parameters"
echo "CI_COMMIT_SHA = $CI_COMMIT_SHA"
echo "CI_REGISTRY = $CI_REGISTRY"
echo "CI_REGISTRY_IMAGE = $CI_REGISTRY_IMAGE"
echo "CI_REGISTRY_PASSWORD = **********"
echo "CI_REGISTRY_USER = $CI_REGISTRY_USER"

print_h2 "Current Docker informations"
docker info

print_h2 "Authenticating against the GitLab registry"
docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

print_h2 "Fetching the latest image if it exists"
docker pull $CI_REGISTRY_IMAGE:latest || true

print_h2 "Building and tagging the image"
docker build --cache-from $CI_REGISTRY_IMAGE:latest --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA .

print_h2 "Pushing the tagged image to the GitLab registry"
docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
