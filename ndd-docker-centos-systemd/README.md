# NDD Docker CentOS systemd

[![pipeline status](https://gitlab.com/ddidier/docker-centos-systemd/badges/master/pipeline.svg)](https://gitlab.com/ddidier/docker-centos-systemd/commits/master)

<!-- MarkdownTOC -->

1. [Introduction](#introduction)
1. [Installation](#installation)
    1. [From source](#from-source)
    1. [From Docker Hub](#from-docker-hub)
1. [Development](#development)
    1. [Testing](#testing)
    1. [Releasing](#releasing)

<!-- /MarkdownTOC -->



<a id="introduction"></a>
## Introduction
A Docker image featuring CentOS with an active *systemd*.

This image was made to test Ansible roles
with [Molecule](https://molecule.readthedocs.io/en/latest/index.html)
and [NDD Ansible Role Template](https://gitlab.com/ddidier/ansible-role-template).



<a id="installation"></a>
## Installation

<a id="from-source"></a>
### From source

```shell
git clone git@gitlab.com:ddidier/docker-centos-systemd.git
cd docker-centos-systemd
docker build -t ddidier/centos-systemd .
```

<a id="from-docker-hub"></a>
### From Docker Hub

```shell
docker pull ddidier/centos-systemd
```



<a id="development"></a>
## Development

<a id="testing"></a>
### Testing

TODO

<a id="releasing"></a>
### Releasing

Do not forget to:

1. update the changelog with all the new features and fixes
2. create the new Docker image
3. run the tests
4. create a Git commit named `Release <CENTOS_VERSION>-<DOCKER_IMAGE_VERSION>`
5. push the master branch to the remote origin
6. tag the last commit with `Release <CENTOS_VERSION>-<DOCKER_IMAGE_VERSION>`
7. push the tag to the remote origin
